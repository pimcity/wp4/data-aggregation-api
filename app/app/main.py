from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from app.api.api import api_router
from dotenv import load_dotenv
import sys
import json
from os.path import join, dirname
import os
from app.utils.db import init_db

sys.path.append("")
load_dotenv(join(dirname(__file__), '.env'))

app = FastAPI(
    title='Data aggregation API', openapi_url="/api"
)

# Set all CORS enabled origins
if os.environ.get("BACKEND_CORS"):
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["http://localhost", "http://localhost:4200", "localhost:4200", "http://localhost:3000", "http://localhost:8080"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

# Set up DB
init_db()

# If swagger's json wants to be exported
@app.on_event("startup")
def save_openapi_json():
    if os.environ.get('EXPORT_SWAGGER_JSON'):
        openapi_data = app.openapi()
        # Change "openapi.json" to desired filename
        with open("openapi.json", "w") as file:
            json.dump(openapi_data, file)


app.include_router(api_router, prefix=os.environ.get("API_STR"))
