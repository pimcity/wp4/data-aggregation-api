from sqlalchemy import Boolean, Column, Integer, String, DateTime, ARRAY
import datetime
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Metadata(Base):
    __tablename__ = "metadata"

    id = Column(Integer, primary_key=True, index=True)
    dataset = Column(String, unique=True, index=True)
    owner = Column(String)
    software = Column(String)
    sensitive = Column(Boolean, default=True)
    anonymized = Column(Boolean, default=True)
    format = Column(String)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    expiring_time = Column(DateTime)
