from sqlalchemy import MetaData
from app.utils.db import get_engine, get_session
from app.models.metadata import Metadata
from fastapi import HTTPException


def get_dataset(dataset):
    # Connect to the DB
    connection = get_engine()
    metadata = MetaData(bind=connection, reflect=True)
    session = get_session()

    # Check if dataset exists in DB
    if not connection.dialect.has_table(connection, dataset):
        raise HTTPException(status_code=404, detail='The dataset you are trying to retrieve does not exist')

    # Get column names of the dataset
    column_names = []
    column_info = session.query(metadata.tables[dataset]).column_descriptions
    for column in column_info:
        column_names.append(column['name'])
    # Wrap column names and data within an array of dictionaries
    final_data = {'data': [], 'metadata': {}}

    for row in session.query(metadata.tables[dataset]).all():
        pair = zip(column_names, row)
        final_data['data'].append(dict(pair))

    # Get metadata and append it to the "final_data" object
    retrieved_metadata = session.query(Metadata).filter(Metadata.dataset == dataset).one()
    final_data['metadata'].update(vars(retrieved_metadata))

    return final_data


def get_datasets():
    session = get_session()
    datasets = []
    for dataset in session.query(Metadata.dataset):
        datasets.append(dataset[0])
    return datasets
