from datetime import date
from pydantic import BaseModel


class Metadata(BaseModel):
    id: int
    dataset: str
    owner: str
    software: str
    sensitive: bool
    anonymized: bool
    format: str
    created_at: date
    expiring_time: date

    class Config:
        orm_mode = True
