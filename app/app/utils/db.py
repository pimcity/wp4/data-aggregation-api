from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os
from sqlalchemy_utils import database_exists
from app.models.metadata import Metadata
import time



def get_engine():
    db_str = 'postgresql://' + os.environ.get('POSTGRES_USER') + ':' + os.environ.get('POSTGRES_PASSWORD') + \
             '@' + os.environ.get('POSTGRES_CONTAINER') + ':' + os.environ.get('POSTGRES_PORT') + \
             '/' + os.environ.get('POSTGRES_DB')

    return create_engine(db_str)


def get_session():
    engine = get_engine()
    session = sessionmaker(bind=engine, expire_on_commit=False)
    return session()


def init_db():
    engine = get_engine()
    # If table don't exist, Create.
    if not engine.dialect.has_table(engine, 'metadata'):
        Metadata.__table__.create(engine)



