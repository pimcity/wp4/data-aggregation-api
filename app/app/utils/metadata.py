import json
from app.models.metadata import Metadata
from app.utils.db import get_session, get_engine
from datetime import datetime
from fastapi import HTTPException


def form_to_model(form: str):
    form = json.loads(form)

    # Check if the name of the dataset may cause problems
    if form['dataset'] == 'metadata' or \
            form['dataset'] == 'dataset' or\
            form['dataset'] == 'datasets':
        raise HTTPException(status_code=422, detail='The dataset name \''+form['dataset'] +
                                                    '\' is incompatible, please change the name of the csv file.')

    # Generate an SQLAlchemy model from the form received by client
    return Metadata(
        dataset=form['dataset'],
        owner=form['owner'],
        software=form['software'],
        sensitive=form['sensitive'],
        anonymized=form['anonymized'],
        format=form['format'],
        created_at=datetime.now(),
        expiring_time=datetime.strptime(form['expiring_time'], '%d/%m/%Y').date()
    )


async def save_metadata(form: str):
    metadata = form_to_model(form)
    # Store the metadata object into the DB
    session = get_session()
    session.add(metadata)
    session.commit()

    return metadata


