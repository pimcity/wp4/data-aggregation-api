# Data Aggregation API

## Intro 

The Data Aggregation (DA) tool enables data owners (for example an Internet Service Provider -ISP that hold a bulk of their users’ data) to perform two important processes on their data: aggregation and anonymization. Such processes enable data owners to share these data in a privacy-preserving way. The DA tool resides on the data owner’s side and its input is the raw data that is available through the initial sources (telco data, sensor data, etc.) and it is transformed in a predefined schema / metadata model. The user (data owner) is responsible for preparing the data for processing (i.e., export from their initial source (internal database), clean them if needed, etc.). Afterwards, through the module, the user is able to choose the subset of the data to be aggregated / anonymized and set the related algorithmic parameters (for aggregation and anonymization). The output is the processed (aggregated / anonymized) data that can be exported to the PIMCity marketplace through an API that the module provides. The data resides on the data owner side and the interested party is able to retrieve them through this API. 

![Architecture](app/docs/screenshots/architecture.png)



## Installation

### Pre-requisites

The machine running Data aggregation API must have installed:

* Python 3.9.2 or superior
* Poetry 1.1.4 or superior
* Uvicorn 0.14.0 or superior

### Deployment

First of all copy the file or rename it from `app/app/.env-example` into `app/app/.env`.

### Docker

A Dockerfile and a docker-compose file have been developed for an easier deployment.

In order to run it, execute:

```
docker-compose --env-file .\app\app\.env build
docker-compose --env-file .\app\app\.env up -d
```
As with the non-docker deployment, to access the application navigate to `http://localhost:5000/`.

### Production deployment
When deploying in the production environment (https://easypims.pimcity-h2020.eu/dashboard/) simply pull from the branch "production" and follow the Docker deployment steps.


## Usage Examples


### Get the list of anonymized datasets

In order to get a name list of anonymized datasets, execute this Http request: `GET http://localhost:5000/api/data/datasets`

You will receive something like this:

`
{
  "datasets": [
  	"Dataset A",
  	"Dataset B",
  	"Dataset C"
  	]
}
`


### Upload and anonymize dataset

In order to upload and anonymize a dataset, execute this Http request: `POST http://localhost:5000/api/data/add`
The body request structure is formed of a `Metadata` Object, which has some additional information regarding the anonymized dataset and the `CSV file`. For more detailed information check the [API documentation](#documentation)
. The request might delay a bit since it doesn't send a response until the dataset if totally anonymized.

### Get the dataset anonymized

To retrieve the anonymized dataset, just execute this request: `GET http://localhost:5000/api/data/{dataset_name}`
In case the dataset exists, the response should be something like:

`
[
	{
		"field_1": "test1",
		"sensitive_field": "****"
		...
	},
	...
]
`

## Documentation

In order to access the API docs navigate to: `http://localhost:5000/docs`

![Docs](app/docs/screenshots/docs.png)

## Changes
- Commit [851948a9](https://gitlab.com/pimcity/wp4/data-aggregation-api/-/commit/851948a96e1fe56d9cd80a2a662fec84e519293c): Merged master to production, added landing page.
- Commit [df7f05](https://gitlab.com/pimcity/wp4/data-aggregation-api/-/commit/df7f05b3b17ba5e7b354ee11d9578e7ec3651feb): If the database is not healthy or has had any error during the container deployment, API won't start.
- Commit [c8c56f](https://gitlab.com/pimcity/wp4/data-aggregation-api/-/commit/c8c56ffad8382f29e3de50da9a2f9ce92f01f55f): Added Hierarchy models and K-anonymity.

## License

The DPC Tool is distributed under AGPL-3.0-only, see the [LICENSE](LICENSE.txt) file.

Copyright (C) 2021 LSTech S.L - Xavi Olivares, Evangelos Kotsifakos
